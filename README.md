# ptpstats conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/ptpstats"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ptpstats module
